import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
			 	<Card className="CardHighlight p-3">
				 <Card.Body>
      				  <Card.Title>Learn From Home</Card.Title>
      				  <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
       				 </Card.Text>
  
      				</Card.Body>
   				 </Card>
			</Col>
			<Col xs={12} md={4}>
			 	<Card className="CardHighlight p-3">
				 <Card.Body className="cardBody">
      				  <Card.Title>Study Now, Pay later</Card.Title>
      				  <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
       				 </Card.Text>
  
      				</Card.Body>
   				 </Card>
			</Col>
			<Col xs={12} md={4}>
			 	<Card className="CardHighlight p-3">
				 <Card.Body>
      				  <Card.Title>Be part of our Community</Card.Title>
      				  <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
       				 </Card.Text>
  
      				</Card.Body>
   				 </Card>
			</Col>
			
		</Row>




		)
}