import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function SampleCourse({courseProp}){

	// console.log(props)
	// console.log(typeof props)
	// console.log(props.courseProp.name)

	/*
	  Use the state hook for this component to be able to stroe its state
	  States are used to keep track of information related to individual components

		Syntax :

			const [getter, setter] = useState(intialGetterValue)


	*/


	const [count, setCount] = useState(0)

	const [seats, countSeats] = useState(30)
	function enroll(){
	
		// if(seat > 0){
		countSeats(seats - 1)
		setCount(count + 1)
		
		// }
		// else{
		// 	return alert('No more Avaiable seat')
		// }
	 }

	 
		useEffect(() => {
			if(seats === 0){
				alert('No more seats available!')
			}
		}, [seats])



	const {name, description, price} = courseProp


	return (

		
				<Card className="SampleCourse p-3">
				 <Card.Body className="BodyCard">
      				  <Card.Title>Sample Course</Card.Title>
      				  
      				  
      				  	<Card.Subtitle>{name}</Card.Subtitle>
          				<Card.Text>{description}</Card.Text>
          				<Card.Subtitle>Price:</Card.Subtitle>
          				<Card.Text>Php {price}</Card.Text>
          				<Card.Text>Enrolless: {count}</Card.Text>
       				 	<Card.Text>Seat: {seats}</Card.Text>
       				 <Button variant="primary" onClick={enroll}>Enroll Now!</Button>
      				</Card.Body>
   				 </Card>
		
		)
}