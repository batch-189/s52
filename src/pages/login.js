import {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'
export default function Login(){


	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	function loginUser(e){
			
		e.preventDefault()

		setEmail("");
		setPassword("")
		


		alert("succefully login!")

	}
	useEffect(() => {
		if(email !== "" && password !== ""){
			

			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [email, password])






	return(
		<Form className="mt-3" onSubmit={(e)=> loginUser(e)}>
			<h1 className="text-center">Login</h1>



		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
			        type="email" 
			        placeholder="Enter email"
			        value={email}
			        onChange={e => {
			        	setEmail(e.target.value)
			        	console.log(e.target.value)
			        }} 
			        required/>

		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        type="password"
		        value={password}
		        onChange={e => {
			        	setPassword(e.target.value)
			        	
			        }} 
		        placeholder="Password" />
		      </Form.Group>


		      {
		      	isActive ?
		      	<Button variant="success" type="submit" id="sumbitBtn">
		        Submit
		      </Button>
		      	:
		      	<Button variant="success" type="submit" id="sumbitBtn" disabled>
		        Submit
		      </Button>
		      }



		      
		      
    </Form>


		)
}