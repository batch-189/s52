// import Navbar from 'react-bootstrap/Navbar'
// import Container from 'react-bootstrap/Container'
// import Nav from 'react-bootstrap/Nav'




import { Navbar, Nav, Container } from 'react-bootstrap';

export default function AppNavbar() {

	return (

		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand href="#home">Batch 189</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="#home">Home</Nav.Link>
		        <Nav.Link href="#courses">Courses</Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

		)

}